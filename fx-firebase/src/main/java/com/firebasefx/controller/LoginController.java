package com.firebasefx.controller;

import java.io.FileInputStream;
import java.io.IOException;

import com.firebasefx.firebase_connection.FirebaseService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/*
    LoginController manages the login UI and interaction with FirebaseService.
*/
public class LoginController extends Application{

    private Stage primaryStage;
    private FirebaseService firebaseService;
    @SuppressWarnings("unused")
    private Scene scene;

    public void setPrimaryStageScene(Scene scene){

        primaryStage.setScene(scene);
    }
    public void intializeLoginScene(){
        Scene logiScene = createLoginScene();
        this.scene = logiScene;
        primaryStage.setScene(logiScene);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
     
        this.primaryStage =primaryStage;
        
        try{
            FileInputStream serviceAccount = new FileInputStream("fx-firebase/src/main/resources/firebasefx.json");
        
            @SuppressWarnings("deprecation")
            FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .setDatabaseUrl("https://fir-fx-79ad6-default-rtdb.asia-southeast1.firebasedatabase.app")
                .build();
            FirebaseApp.initializeApp(options);

        }catch(IOException e){
            e.printStackTrace();
        }

        // create initial login scene

        Scene scene = createLoginScene();
        this.scene = scene;
        primaryStage.setTitle("Firebase Auth Example");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /*
        Create the login scene.
        @return the Scene object representing the login UI
    */

    private Scene createLoginScene(){
        TextField emailField = new TextField();
        emailField.setPromptText("Email");

        PasswordField passwordField = new PasswordField();
        passwordField.setPromptText("Password");

        Button signUpButton = new Button("Sign Up");
        Button loginButton = new Button("Login In");
        
        firebaseService = new FirebaseService(this, emailField, passwordField);

        signUpButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                firebaseService.signUp();
            }
        });

        loginButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                firebaseService.login();
            }
        });

        VBox fieldBox = new VBox(20, emailField, passwordField);
        HBox bottonBox = new HBox(20, loginButton, signUpButton);
        VBox comBox = new VBox(10, fieldBox,bottonBox);
        Pane viewPane = new Pane(comBox);

        return new Scene(viewPane, 400, 200);
    }
}
