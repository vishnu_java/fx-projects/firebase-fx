package com.firebasefx.firebase_connection;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.firebasefx.controller.LoginController;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/*
    FireBaseSeervice handles Firebase authentication operations like signup, login, logout
*/
public class FirebaseService {
 
    private TextField emailField;
    private PasswordField passwordField;
    private LoginController loginController;

    /*
     */

    public FirebaseService(LoginController loginController, TextField emailField, PasswordField passwordField){
        this.loginController = loginController;
        this.emailField = emailField;
        this.passwordField = passwordField;

    }
    /*
    
    */

    public boolean signUp(){
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            // Create user request
            UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(email)
                .setPassword(password)
                .setDisabled(false);
            
            // Create user in Firebase Auth
            UserRecord userRecord = FirebaseAuth.getInstance().createUser(request);
            System.out.println("Successfully created user: "+ userRecord.getUid());
            showAlert("Success" ,"User created successfully.");
            return true;    

        } catch (FirebaseAuthException e) {
            
            e.printStackTrace();
            showAlert("Error", "Failed to create user: "+ e.getMessage());
            return false;
        }
    }

    /*
    
    */
    public boolean login(){
        String email = emailField.getText();
        String password = passwordField.getText();

        try {
            // Firebase API key 
            String apiKey = "AIzaSyCjRt5S9cFSduJyqm5l7rPE5vXYm-ByyQg";

            // Firebase Authentication URL
            URL url = new URL("https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key="+ apiKey);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/json; charset-UTF-8");
            conn.setDoOutput(true);

            // body for login

            JSONObject jsonRequest = new JSONObject();
            jsonRequest.put("email", email);
            jsonRequest.put("password", password);
            jsonRequest.put("returnSecureToken", true);

            // Send request

            try(OutputStream os = conn.getOutputStream()){

                byte[] input = jsonRequest.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }
            if (conn.getResponseCode()== 200) {
                
                showAlert(true);
                return true;
            } else {

                showAlert("Invalid Login", "Invalid Credentials!!!");
                return false;
            }
        } catch (Exception e) {
            
            e.printStackTrace();
            showAlert(false);
            return false;
        }
    }
    /*
    
    */

    private void showAlert(String title, String massage){

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(massage);
        alert.showAndWait();
    }

    /*
    
    */

    private void showAlert(boolean isLoggedIn){

        Label msg = new Label("Welcome" );
        msg.setAlignment(Pos.CENTER);

        Button logOutButton = new Button("Logout");

        VBox vBox = new VBox(100,msg,logOutButton);

        logOutButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                loginController.intializeLoginScene();
            }
        });

        Scene scene = new Scene(vBox, 400,200);
        loginController.setPrimaryStageScene(scene);
    }
}
